# Simple Automation Demonstration

## Overview

This demonstration repository will use GitLab CI/CD to execute the following steps to deploy the HTTPbin Headers API to Kong Konnect:

* Installs decK, Inso CLI, Jq and Yq
* Lints the OpenAPI Specification (OAS)
* Generate Kong declarative configuration from OAS using decK
* Validates Kong declarative config using decK
* Diffs declarative config using decK
* Backup existing Kong configuration using decK
* Deploys declarative config to Konnect Control Plane using decK
* Creates a new API Product
* Uploads Markdown Documentation using Konnect Admin API
* Runs Unit Tests from a script
* Creates a new API Product Version
* Uploads OAS to Product Version using Konnect Admin API
* Publishes API Product Version to Developer Portal using Konnect Admin API

## Usage

Create the following variables for the repository:

| Variable Name              | Description                                                           | Example Value(s)                                                                     |
|----------------------------|-----------------------------------------------------------------------|--------------------------------------------------------------------------------------|
| API_PRODUCT_PUBLISH        | Should the pipeline publish the API Product to the developer portal?  | true/false                                                                           |
| API_PRODUCT_VERSION_STATUS | Product version lifecycle stage                                       | published/deprecated/unpublished                                                     |
| GATEWAY_URL                | The Kong Gateway URL                                                  | https://ada26de24b0094f5a8cc3b7ecdd6a4a2-1104373322.eu-west-2.elb.amazonaws.com:8443 |
| KONNECT_ADDRESS            | The Konnect Endpoint, including the region                            | https://eu.api.konghq.com                                                            |
| KONNECT_CONTROL_PLANE_NAME | Name of the Control Plane that we are deploying to                    | team1-prod                                                                           |
| KONNECT_PORTAL             | UUID of the Konnect Developer Portal                                  | 4abacaf1-47dc-4c07-83ff-a8801782277f                                                 |
| KONNECT_TOKEN              | Konnect Access Token used for authenticating with the API             | kpat_iHSS7rjpa5eWsHV9Eu5UWfzXxWSIFnJycoas65E1fz2AzSbh5                               |
